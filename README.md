# VyOS Router driver/connector/communicator
## :electric_plug: Area of Expertise: (Python for) Network engineering
During my internship at [Wargaming](https://wargaming.com/en/), among others, I developed a tool that allows the IT department to connect to various routers (specialized for VyOS) in order to 
- Get information
- Change information
- Run commands
- And in general, execute all VyOS or other router available commands via **this application**

## :computer: Technologies used
- Python
- [Netmiko library](https://pypi.org/project/netmiko/)
  - Paramiko

## :keyboard: Available execution options
- Execute a command using default connection
- Open a connection (specified in command line) and ask for commands
- Show Manual
- Connect to device using a json file

### Extra features
- :floppy_disk: Save output of interaction with Router and results provided by Router in a file placed in your computer

## :gear: How to use
#### Execute a single command using default connection
```python
python netmiko_driver.py default <command>
```
or
```python
python netmiko_driver.py auto <command>
```

#### Connect using command line
```python
python netmiko_driver.py <username> <password> <host>
```
or
```python
python netmiko_driver.py
```
In this case a password will be asked to be provided and the software will use default username and host

#### Connect using a json file
A json file should have the following structure
```json
[
	{
		"username":"ubnt",
		"password":"ubnt",
		"host":"192.168.1.1",
		"device_type":"vyos"
	}
]
```

#### Show manual
Any of the following commands will display the manual
```python
python netmiko_driver.py man
```
```python
python netmiko_driver.py -h
```
```python
python netmiko_driver.py help
```
```python
python netmiko_driver.py --help
```
