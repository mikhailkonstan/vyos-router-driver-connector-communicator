#!/usr/bin/env python
from netmiko import ConnectHandler
from getpass import getpass
import sys
import json

def connect_test(username='ubnt' , password='ubnt' , device_type='vyos' , host='192.168.1.1'):
	''' For testing purposes the function connects to host and executes 'ls' command. Prints output 
	result and then disconnects '''

	net_connect = ConnectHandler(username=username , password=password , device_type=device_type , host=host)
	output=net_connect.send_command("ls")
	print(output)
	net_connect.disconnect()

def open_connection(username , password , host , device_type='vyos'):
	''' Establishes a connection and returns the connection as a variable '''
	try:
		net_connect = ConnectHandler(username=username , password=password , device_type=device_type , host=host)
		return net_connect
	except:
		errormsg = "Error occured:" , sys.exc_info()[0]
		print(errormsg)
		return None

def close_connection(net_connect):
	''' Disconnects 
	@net_connect: Variable/Object that is connected '''

	net_connect.disconnect()

def execute_command(net_connect , cmd):
	''' Executes given command in given connection(net_connect) '''

	try:
		output = net_connect.send_command(cmd , delay_factor=1)
		print(output)
		return output
	except:
		errormsg = "Error occured:" , sys.exc_info()[0]
		print(errormsg)
		return errormsg

def fun_commands():
	#Loop
	cmd = ''
	while True:
		cmd = input('Command:')
		#Check if user wants to exit
		if(cmd=='end' or cmd=='exit'):
			break
		#Check if user asked for manual
		if(cmd=='man' or cmd=='help' or cmd=='h'):
			show_manual()
		#Check if user wants to save output to file and execute command to router
		else:
			tmpcmd = cmd.split(' ')
			if(len(tmpcmd) > 2 and tmpcmd[-2]=='-s'):
				isolatedcommand = get_command_form_args(tmpcmd , -2) #ignore last 2 elements
				results = execute_command(connection , isolatedcommand)
				write_to_file(results , tmpcmd[-1])
			elif(len(tmpcmd) > 1 and tmpcmd[-2]=='-f'):
				cmds = get_commands_from_file(tmpcmd[-1])
				for cmd in cmds:
					print("Executing command: " , cmd)
					execute_command(connection , cmd)
			else:
				execute_command(connection , cmd)
			
			
	#end loop

def show_manual():
	''' Prints to screen help manual '''

	f = open("drivermanual.txt", "r")
	print(f.read()) 
	f.close()

def write_to_file(output , filename):
	''' Writes output parameter to given filename and changes line '''

	file = open(filename,"a")
	file.write(output + '\n')
	file.close()

def _copy_from_index(array , index):
	toret = []
	if(index>0):
		for x in range(index , len(array)):
			toret.append(array[x])
	else:
		for x in range(0 , len(array) + index):
			toret.append(array[x])
	return toret

def get_command_form_args(array , index=2):
	''' Isolates command from other variables/commands in arguments line
	@index: Elements to ignore ex. 2 ignore first 2 elements '''

	tmparray = _copy_from_index(array , index)
	toret= ' '.join(tmparray)
	return toret

def get_commands_from_file(filename='commands.txt'):
	try:
		lines = [line.rstrip('\n') for line in open(filename)]
		return lines
	except: 
		print("Error occured:" , sys.exc_info()[0])
		return "no command"

def get_config_from_json(filename='hosts.json'):
	with open(filename) as dev_file:
		toret =  json.load(dev_file)
	return toret
	
#main

connection = None
#Option 1 - Execute command using default connection
if(len(sys.argv) > 1 and (sys.argv[1]=='default' or sys.argv[1]=='auto')):
	#Connect using default options, execute command and close program
	connection = open_connection('ubnt' , 'ubnt' , '192.168.1.1')
	cmd = get_command_form_args(sys.argv)
	execute_command(connection , cmd)
	close_connection(connection)
	exit()

#Option 2 - Show manual
if(len(sys.argv) > 1 and (sys.argv[1]=='man' or sys.argv[1]=='-h' or sys.argv[1]=='help' or sys.argv[1]=='--help')):
	show_manual()
	exit()
#Option 3 - Connect by json file
elif(len(sys.argv) > 2 and (sys.argv[1]=='json')):
	devices = get_config_from_json(sys.argv[2])
	for device in devices:
		connection = ConnectHandler(**device)
#Option 4 - Open connection and ask for commands
elif(len(sys.argv) > 3):
	connection = open_connection(sys.argv[1], sys.argv[2], sys.argv[3])
else:
	connection = open_connection('ubnt' , getpass() ,'192.168.1.1')
fun_commands()
close_connection(connection)