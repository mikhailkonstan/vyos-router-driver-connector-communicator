import logger
import unittest

class TestLogger(unittest.TestCase):
	def test_print_error(self):
		self.assertEqual(logger.print_error('ftest' , 'test exception'), 'Error in ftest(): Exception is test exception')
		self.assertEqual(logger.print_error('ftest' , 'NetMikoTimeoutException'), 'Error in ftest(): Could not connect to device in default time(timeout error)')
		self.assertEqual(logger.print_error('ftest' , 'NetMikoAuthenticationException'), 'Error in ftest(): Could not connect to device because the credentials you used are wrong')
		self.assertEqual(logger.print_error('ftest' , 'AttributeError'), 'Error in ftest(): An attribute is not correctly assigned. Perhaps a connection is not established')
		self.assertEqual(logger.print_error('ftest' , 'FileNotFoundError'), 'Error in ftest(): You are trying to use a file that is not found')
		self.assertEqual(logger.print_error('ftest' , 'ValueError'), 'Error in ftest(): The input is incorrect.')
		self.assertEqual(logger.print_error('ftest' , 'KeyboardInterrupt'), 'Error in ftest(): Operation is terminated due to user intervension')

	def test_print_warning(self):
		self.assertEqual(logger.print_warning('ftest' , 'test exception'), 'Warning in ftest(): Exception is test exception')
	def test_print_debug(self):
		self.assertEqual(logger.print_debug('ftest' , 'test message'), 'Debug in ftest(): test message')
	def test_print_info(self):
		self.assertEqual(logger.print_info('ftest' , 'test message'), 'Info in ftest(): test message')	

if __name__ == '__main__':
	unittest.main()