from getpass import getpass
import sys

from routerlib import *

#main
connection = None
#Option 1 - Execute command using default connection
if(len(sys.argv) > 1 and (sys.argv[1]=='default' or sys.argv[1]=='auto')):
	#Connect using default options, execute command and close program
	connection = open_connection('ubnt' , 'ubnt' , '192.168.1.1')
	cmd = get_command_form_args(sys.argv)
	decode_command_and_execute(connection , cmd)
	close_connection(connection)
	exit()

#Option 2 - Show manual
if(len(sys.argv) > 1 and (sys.argv[1]=='man' or sys.argv[1]=='-h' or sys.argv[1]=='help' or sys.argv[1]=='--help')):
	show_manual()
	exit()
#Option 3 - Connect by json file
elif(len(sys.argv) > 2 and (sys.argv[1]=='json')):
	devices = get_config_from_json(sys.argv[2])
	for device in devices:
		connection=open_connection_json(device)
	if(len(sys.argv) > 3):
		cmd = get_command_form_args(sys.argv , 3)
		decode_command_and_execute(connection , cmd)
		close_connection(connection)
		exit()
#Option 4 - Open connection and ask for commands
elif(len(sys.argv) > 3):
	connection = open_connection(sys.argv[1], sys.argv[2], sys.argv[3])
else:
	connection = open_connection('ubnt' , getpass() ,'192.168.1.1')
#commands receiver - CLI
fun_commands(connection)
close_connection(connection)


