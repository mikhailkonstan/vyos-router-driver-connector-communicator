#!/usr/bin/env python
from netmiko import ConnectHandler
import netmiko
import sys
import json

from logger import print_error

def open_connection(username, password, host, device_type='vyos'):
	''' Establishes a connection and returns the connection as a variable 
		
	Parameters:
		username: username
		password: password
		host: Address of device(ex. 192.168.1.1)
		device_type: Default value is vyos. Defines what is the operation system of the device
	
	Returns:
		connection: a netmiko ConnectHandler
	'''
	try:
		net_connect = ConnectHandler(username=username, password=password, device_type=device_type, host=host)
		return net_connect
	#Call logger, send function name and exception error)
	except:
		print_error(sys._getframe().f_code.co_name, str(sys.exc_info()[0].__name__))
	return None

def open_connection_json(file):
	''' Establishes a connection using a given json file and returns the connection as a variable 

	Parameters:
		file: A file in json format to read

	Returns:
		connection: a netmiko ConnectHandler
	'''
	try:
		net_connect = ConnectHandler(**file)
		return net_connect
	except:
		print_error(sys._getframe().f_code.co_name, str(sys.exc_info()[0].__name__))
	return None
	
def close_connection(net_connect):
	''' Disconnects from device

	Parameters:
		net_connect: Variable/Object that is already connected

	Returns:
		connection: a netmiko ConnectHandler
	'''

	try:
		net_connect.disconnect()
	#Call logger, send function name and exception error)
	except:
		print_error(sys._getframe().f_code.co_name, str(sys.exc_info()[0].__name__))

def given_save_file(array):
	''' Check if user prompted -s parameter 

	Parameters: 
		array: An array of commands and/or parameters

	Returns 
		True whether -s parameter is given
	'''
	return (len(array) > 2 and array[-2]=='-s')

def given_execution_file(array):
	''' Check if user prompted -f parameter
	
	Parameters: 
		array: An array of commands and/or parameters

	Returns 
		True whether -f parameter is given
	'''
	return (len(array) > 1 and array[-2]=='-f')

def execute_command(net_connect, cmd):
	''' Executes given command in given connection(net_connect) 

	Parameters:
		net_connect: Variable/Object that is already connected
		cmd: Command to be executed

	Returns:
		The results from the execution
	'''

	try:
		output = net_connect.send_command(cmd, delay_factor=1)
		print(output)
		return output
	#Call logger, send function name and exception error)
	except:
		print_error(sys._getframe().f_code.co_name, str(sys.exc_info()[0].__name__))
		return 'errormsg'

def execute_command_from_file(connection, array):
	''' Executes all commands written on a file

	Parameters:
		connection: Variable/Object that is already connected to device
		array: An array of commands and/or parameters

	Returns:
		All command execution results in a form of a list
	'''
	cmds = get_commands_from_file(array[-1])
	toret = []
	for cmd in cmds:
		print('Executing command: ', cmd)
		toret.append(execute_command(connection, cmd))
	return toret;

def decode_command_and_execute(connection, cmd):
	'''	Decodes given command(s) and then executes according to any possibly given parameters

	Parameters:
		connection: Variable/Object that is already connected to device
		cmd: A string of given commands and/or parameters
		
	Returns 
		Function does not return anything
	'''

	tmpcmd = cmd.split(' ')
	#Check if given -s parameter
	if(given_save_file(tmpcmd)):
		isolatedcommand = get_command_form_args(tmpcmd, -2) #ignore last 2 elements
		newtmpcmd = isolatedcommand.split(' ');
		
		#Check if given -f parameter
		if(given_execution_file(newtmpcmd)):
			results = execute_command_from_file(connection, newtmpcmd)
			for res in results:
				write_to_file(res, tmpcmd[-1])
		else:
			results = execute_command(connection, isolatedcommand)
			write_to_file(results, tmpcmd[-1])
	
	#Check if given -f parameter
	elif(given_execution_file(tmpcmd)):
		execute_command_from_file(connection, tmpcmd)
	else:
		execute_command(connection, cmd)

def fun_commands(connection):
	''' Function that handles user input and according to that, calls device

	Parameters:
		connection: A variable/object that contains a connected to device netmiko connectHandler

	Returns:
		Function does not return anything
	'''
	#Loop
	cmd = ''
	while True:
		cmd = input('Command:')
		#Check if user wants to exit
		if(cmd=='end' or cmd=='exit'):
			break
		#Check if user asked for manual
		if(cmd=='man' or cmd=='help' or cmd=='h'):
			show_manual()
		#Check for more complicated aspects like parameters
		else:
			decode_command_and_execute(connection, cmd)
	#end loop

def show_manual():
	''' Prints to screen help manual 

	Returns:
		Function does not return anything
	'''

	f = open('drivermanual.txt', 'r')
	print(f.read()) 
	f.close()

def write_to_file(input, filename):
	''' Writes output parameter to given filename and changes line 

	Parameters:
		input: Data to write in file
		filename: File to write the data

	Returns: 
		Function does not return anything
	'''
	try:
		file = open(filename,'a')
		file.write(input + '\n')
		file.close()
	except:
		print_error(sys._getframe().f_code.co_name, str(sys.exc_info()[0].__name__))

def _copy_from_index(array, index):
	''' Helping function that copies an array from a given index. If the index is negative then it copies an array
	from 0 until the given index. If index is equal to zero function is bypassed
	
	Parameters:
		array: array/list to copy
		index: can be positive or negative number to copy from/to the elements of array

	Returns: 
		A new list with only the elements starting or ending to the given _copy_from_index
	'''
	if(index == 0):
		return array
	toret = []
	if(index > 0):
		for x in range(index, len(array)):
			toret.append(array[x])
	else:
		for x in range(0, len(array) + index):
			toret.append(array[x])
	return toret

def get_command_form_args(array, index=2):
	''' Isolates command from other variables/commands in arguments line
	
	Parameters:
		index: Elements to ignore ex. 2 ignore first 2 elements 

	Returns: 
		A string that contains commands and/or parameters only
	'''

	tmparray = _copy_from_index(array, index)
	toret= ' '.join(tmparray)
	return toret

def get_commands_from_file(filename='commands.txt'):
	''' Reads a file to the end and gets the commands located in it

	Parameters:
		filename: File that contains commands(Default value is commands.txt)

	Returns:
		Returns commands as list
	'''

	try:
		lines = [line.rstrip('\n') for line in open(filename)]
		return lines
	#Call logger, send function name and exception error)
	except:
		print_error(sys._getframe().f_code.co_name, str(sys.exc_info()[0].__name__))
		return ' '

def get_config_from_json(filename='hosts.json'):
	''' Gets all configuration parameters saved in a json file 

	Parameters:
		filename: File that contains configuration parameters

	Returns:
		Returns an object containing all information read from the json file
	'''
	
	try:
		with open(filename) as dev_file:
			toret =  json.load(dev_file)	
		return toret
	except:
		return None
	
	