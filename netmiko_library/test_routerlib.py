import routerlib
import unittest

class TestLogger(unittest.TestCase):
	def test_config_from_json(self):
		jsonobj = [{
			"username":"ubnt",
			"password":"ubnt",
			"host":"192.168.1.1",
			"device_type":"vyos"
		}]
		self.assertEqual(routerlib.get_config_from_json("hosts.json") , jsonobj)
		self.assertEqual(routerlib.get_config_from_json() , jsonobj)
		self.assertEqual(routerlib.get_config_from_json("hostswrong.json") , None)

if __name__ == '__main__':
	unittest.main()