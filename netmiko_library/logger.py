def _get_message(exception):
	msg = {
		"NetMikoTimeoutException":"Could not connect to device in default time(timeout error)",
		"NetMikoAuthenticationException":"Could not connect to device because the credentials you used are wrong",
		"AttributeError":"An attribute is not correctly assigned. Perhaps a connection is not established",
		"FileNotFoundError":"You are trying to use a file that is not found",
		"ValueError":"The input is incorrect.",
		"KeyboardInterrupt":"Operation is terminated due to user intervension"
	}
	errormsg = msg.get(exception, "null")
	if(errormsg=="null"):
		errormsg = "Exception is " + str(exception)
	return errormsg

def print_error(funcname , exception):
	''' Prints error message and exception description to the screen '''
	str =  "Error in " + funcname + "(): " + _get_message(exception)
	print(str)
	return str

def print_warning(funcname , exception):
	''' Prints warning message and exception description to the screen '''
	str =  "Warning in " + funcname + "(): " + _get_message(exception)
	print(str)
	return str

def print_debug(funcname , message):
	''' Prints debug message to the screen '''
	str =  "Debug in " + funcname + "(): " + message
	print(str)
	return str

def print_info(funcname , message):
	''' Prints information message to the screen '''
	str =  "Info in " + funcname + "(): " + message
	print(str)
	return str